# Pristy User Documentation

## Compilation


### Avec mise

```shell
#Uv est utilisé comme python package manager
mise use -g uv@latest

#Appel la task mise configuré dans mise.toml
mise run install
```

### Avec Direnv

```shell
echo 'layout python3' > .direnv
direnv allow
pip install --upgrade pip
pip install -r requirements.txt
```

### Manuel

```shell
python3 -m venv .venv
source .venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```


## Démarrage

```shell
mkdocs serve
```
Accès à la documentation sur http://127.0.0.1:8000/

ou

```shell
mkdocs serve --dirtyreload
```
Pour un build plus rapide, mais avec risque de liens non fonctionnels.

### Sur Docker

```shell
docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material
```
Accès via : http://localhost:8000/

### Avec mise

```shell
mise run build 
mise run serve
```

## navigation

Dans mkdocs.yml sous la section nav.
https://www.mkdocs.org/user-guide/writing-your-docs/#configure-pages-and-navigation


# Mise à jour de contenu :
Penser à mettre à jour le menu dans
'''\~/mkdocs.yalm/#nav'''

Et à mettre à jour le plan du site
'''\~/docs/plan-du-site/'''

# Notes

- le logo soit être placé dans le dossier 'assets' qui doit être dans le dossier 'docs'
- le favicon doit se trouvé dans le dossier 'images' du dossier 'assets'


---

# Pour garder une cohérence

Voici une parti des shortcodes employés régulièrements, merci de vous y référez en cas de corrections ou ajouts et d'alimenter cette parti si d'autres sont ajoutés.

ATTENTION : La mise en page n'est pas rendue dans le READ ME.

## Visuels

### Boutons d'actions

[Essayez Pristy](https://pristy.fr/demo){ .md-button }






## Autres mise en page disponibles
### Tableau basic

| Method      | Description                          |
| :---------- | :----------------------------------- |
| `GET`       | :material-check:     Fetch resource  |
| `PUT`       | :material-check-all: Update resource |
| `DELETE`    | :material-close:     Delete resource |


### Tableau à onglets

=== "Tableau à onglets"
    Contenu onglet 1

=== "Onglets 2"

      Contenu onglet 2

=== "Onglets 3"

      Contenu onglet 3

=== "Onglets 4"

      Contenu onglet 4

## Licensing

Copyright 2025 - Jeci SARL - https://jeci.fr

Pristy Documentation is free software; you can copy, distribute and/or modify it
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".
