---
title: Présentation de Pristy
description: Présentation de Pristy et ses applications
---
<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->
# Présentation de l'application Pristy

Pristy est une suite d'applications métier qui propose une interface alternative à Alfresco Share ou Alfresco App Content (ACA). Pristy est une alternative aux logiciels de GED de _drive_ américains.

Le but de Pristy : rester compatible à Alfresco tout en proposant des interfaces modernes et intuitives, simple à utiliser.

Pour suivre l'avancée de l'application rendez-vous sur [notre roadmap](https://pristy.fr/fr/roadmap/).

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Presentation Pristy Espaces](../espaces/presentation.md)
    - [Presentation Pristy Portail](../portail/presentation.md)
    - [Presentation Pristy Actes](../actes/presentation.md)

!!! info inline end "Pour aller plus loin :"
    - [Bien débuter avec Pristy](bien-debuter.md)

<!-- end Liens de bas de page -->
