---
title: Bien débuter dans Pristy
description: Se repérer dans Pristy
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Se repérer et naviguer dans Pristy

<!-- Prérequis -->

!!! info inline "Présent dans l'application"
    - [x] Pristy Espaces
    - [x] Pristy Actes
    - [x] Pristy Portail


!!! info inline end "Rôle nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
    - [ ] Administrateur

---
!!! check "Pré-requis"
    - Avoir un compte sur une instance Pristy     

<br> 
<br> 
<!-- end Prérequis -->

## Se repérer dans Pristy

Toutes les applications ont le même enrobage : 

- En haut se situe une bannière composée
  - à gauche d'un logo qui renvoi vers Pristy Portail
  - à droite de boutons d'actions :
    - la recherche <i class="ri-search-line"></i>
    - les paramètres :octicons-gear-16:, incluant la corbeille
    - la page profil :octicons-person-24:
    - l'action de déconnexion :fontawesome-solid-arrow-right-from-bracket:
- Au centre se situe le contenu
- Sur la gauche se situe un menu qui peut être ouvert ou fermé grâce à l'icône hamburger :material-menu: (en haut à gauche du logo)

## Première connexion

Lors de votre première connexion, vous serez renvoyer vers la page d'accueil de Pristy Portail, la porte d'entrée de toutes vos applications Pristy.

Dans le menu :material-menu:, vous trouverez les applications auxquels vous avez accès, cliquez dessus pour accéder à votre application.


!!! tip "Accès rapide"
    Si vous voulez accès rapidement et fréquemment à une application spécifique, ajoutez-la aux favoris de votre navigateur !

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Presentation Pristy](presentation.md)
    - [Se repérer dans Pristy Portail](../portail/bien-debuter.md#se-reperer-dans-pristy-portail)



!!! info inline end "Pour aller plus loin :"
    - [Se repérer dans Pristy Espaces](../espaces/bien-debuter.md#se-reperer-dans-pristy-espaces)
    - [Se repérer dans Pristy Actes](../actes/bien-debuter.md#se-reperer-dans-pristy-actes)

<!-- end Liens de bas de page -->

[//]: # (TODO 24-01-31 : Installer Remix Icon pour MAJ icons)
