---
title: "Accéder à Pristy"
description: Connexion/Déconnexion sur Pristy Portail
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Accéder au compte : Connexion/Déconnexion

<!-- Prérequis -->

!!! info inline "Présent dans l'application"
    - [x] Pristy Espaces
    - [x] Pristy Actes
    - [x] Pristy Portail


!!! info inline end "Droits nécessaires"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
    - [ ] Administrateur

---
!!! check "Pré-requis"
    - Avoir un compte sur une instance Pristy     

<br> 
<br> 
<!-- end Prérequis -->

## Se connecter

Cette étape suppose que vous avez fait une demande de compte et que l'administrateur vous a fourni un *url de connexion*, votre *nom utilisateur* et votre *mot de passe*.

Si vous avez tous cela, vous pouvez suivre les instructions suivant pour vous connecter à votre Pristy :

- Accéder à votre accès Pristy via l'url qui vous a été fournie, elle ressemble à `https://votre-structure.pristy.net/portail`
- Utiliser les identifiants fournis par votre administrateur.
- Saisir votre nom utilisateur dans la partie *identifiant*
- Saisir votre mot de passe dans la partie *mot de passe*
- Cliquer sur "Se connecter"

!!! note "Identifiants perdus ou oubliés"
      Si vous ne les avez pas reçus ou si vous les avez égarés, il faudra en (re)faire la demande.

## Se déconnecter

Pour vous déconnecter :

- Cliquer en haut à droite sur l'icône "Déconnexion"

!!! note "À savoir"
      Pour créer un compte, changer votre nom utilisateur ou le mail de votre compte, vous devez contacter votre administrateur.

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Presentation Pristy](presentation.md)
    - [Bien débuter avec Pristy](bien-debuter.md)


!!! info inline end "Pour aller plus loin :"
    - [Bien débuter avec Pristy](bien-debuter.md)


<!-- end Liens de bas de page -->

[//]: # (TODO 24-01-31 - note : vidéos avec ancienne interface & icons)
