---
title: Gestion des membres
description: Gestion des membres d'un espaces de travail sur Pristy Espaces
---


<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [X] Pristy Espaces
    - [X] Pristy Actes
    - [ ] Pristy Portail

!!! check inline end "[Rôle](roles.md) nécessaire pour ajouter/supprimer un utilisateur/groupe"
    - [x] Utilisateur Gestionnaire
    - [ ] Utilisateur Éditeur
    - [ ] Utilisateur Lecteur

---

!!!info "Pré-requis"
    Cette étape suppose que vous vous êtes connecté à Pristy et avez accès à au moins un espace de travail dont vous avez le rôle Gestionnaire.


<br>
<!-- end Prérequis -->

## Accès à la gestion des membres
La gestion des membres peut se faire en allant directement dans la page dédiée, mais aussi à la création d'un espace.
Pour aller sur la page, vous devez :

* Dans la page listant les espaces de travail, repérer l’espace de travail où vous voulez gérer les membres
* Cliquer sur le menu “Plus d’actions”, représenté par trois points verticaux :material-dots-vertical:, sur l'espace de votre choix
* Cliquer sur ":octicons-person-24: Gérer les membres" 

Vous avez maintenant accès à la page. Dans cette page, vous pouvez voir la liste des utilisateurs et groupes membres de l'espace de travail. Vous pouvez dans cette liste trier sur le groupe.
En dessous de la liste, vous retrouverez la partie pour rechercher et ajouter un utilisateur ou groupe à l'espace de travail.

## Ajouter un membre

Pour ajouter un utilisateur dans un espace de travail, vous devez :

* Sélectionner un utilisateur dans la barre de recherche 
  * Vous pouvez chercher un membre via son nom, prénom ou mail.
* Choisir le [rôle](roles.md) que vous voulez attribuer (Lecteur, Éditeur ou Gestionnaire)
* Cliquer sur le bouton ajouter

L'utilisateur est membre de l'espace avec le rôle choisi et peut maintenant accéder à son contenu. 


## Ajouter un groupe d'utilisateurs

L'ajout d'un groupe se fait de la même façon que pour ajouter un utilisateur sauf que dans le menu déroulant, à la place de sélectionner un utilisateur, vous allez sélectionner un groupe (un icône :octicons-people-24:{ .revert-image } représentant un groupe est présent pour les différencier des utilisateurs).

!!! note "À savoir"
    Un utilisateur peut être ajouté plusieurs fois (directement en tant qu'utilisateur ou sinon en tant que membre d'un groupe). Cependant vous ne le verrez apparaître deux fois que si le rôle de son groupe et celui qui lui a été affecté sont différents.

## Changer le rôle d'un membre ou d'un groupe d'utilisateurs

Pour changer le rôle d'un utilisateur dans un espace de travail :

* Repérer l'utilisateur (:octicons-person-24:) ou le groupe (:octicons-people-24:{ .revert-image }) dont vous souhaitez changer le rôle dans la liste des membres de l'espace de travail
* Choisissez un rôle dans le menu déroulant du rôle

L'utilisateur a changé de rôle dans l'espace choisi. 

## Supprimer un membre d'un espace

Pour supprimer un utilisateur membre d'un espace de travail, vous devez :

* Repérer l'utilisateur que vous souhaitez supprimer dans la liste des membres de l'espace de travail
* Cliquer sur la croix rouge se situant au bout de la ligne correspondant à l'utilisateur

Une fois cela fait, vous pouvez observer que l'utilisateur a été supprimé de la liste des membres de l'espace.

## Supprimer un groupe d'utilisateurs

La suppression d'un groupe se fait de la même façon que pour supprimer un utilisateur, mais cliquant sur la croix de la ligne correspondant à un groupe.

<!-- à ajouter quand dev fini : Repérer par le logo users -->

!!! warning "À savoir"
    La suppression d'un groupe supprime automatiquement les membres du groupe de l'espace de travail. Si le groupe et l'utilisateur ont été ajouté avec la même permission, vous verrez alors toujours l'utilisateur dans la liste des membres.

---

!!! note "À savoir"
    Les utilisateurs membres de l'espace en tant qu'éditeurs ou lecteurs peuvent consulter les membres à défaut de pouvoir les gérer.



<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Rôles dans Pristy](roles.md)
    - [Se repérer dans Pristy Espaces](../espaces/bien-debuter.md#se-reperer-dans-pristy-espaces)


!!! info inline end "Pour aller plus loin :"
    - [Mes Espaces](../espaces/mes-espaces.md)
    - [Administrer Pristy](../portail/administration.md)
    - [Administrer vos espaces Actes](../actes/administration.md)

<!-- end Liens de bas de page -->

[//]: # (TODO 24-01-31 : après 'sprint fin janvier - S9' update avec les nouveautés)
