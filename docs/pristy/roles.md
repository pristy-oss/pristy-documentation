---
title: Rôles dans Pristy
description: À propos de la notion de rôle dans Pristy.
---


<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Rôles

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [X] Pristy Espaces
    - [X] Pristy Actes
    - [ ] Pristy Portail

!!! check inline end "Rôle nécessaire pour gérer les rôles d'un espace"
    - [x] Utilisateur 
    - [ ] Administrateur interne

---

!!!info "Pré-requis"
    - aucun

<br>
<br>
<!-- end Prérequis -->

## Rôles dans Pristy

Il existe plusieurs rôles dans Pristy, celui d'installateur et de mainteneur, celui d'administrateur interne, celui d'utilisateur. 

- L'installateur est la personne qui a installé Pristy
- Le mainteneur est la personne en charge qui fait les mises à jour, et qui résout les bugs remontés.
- L'administrateur interne est la personne dans la structure responsable d'administrer Pristy
- Les utilisateurs sont les personnes qui utilisent Pristy

Certaines actions, ne sont pas réalisables par l'utilisateur, ce dernier doit donc se rapprocher de son administrateur pour agir.  


## Rôle dans un espace

Dans Pristy, il existe 3 rôles : Gestionnaire, Éditeur et Lecteur.

Chacun de ses rôles s'attribuent dans la page de gestion des membres d'un espace. Les rôles définissent les autorisations de l'utilisateur à consulter, modifier ou déplacer un dossier ou un document.

---
!!! check inline "Rôle"
    - [x] Gestionnaire
    - [ ] Éditeur
    - [ ] Lecteur

Un **gestionnaire** a tous les pouvoirs sur son contenu, il peut le créer, le modifier , le déplacer, le partager et l'organiser ou le réorganiser à sa guise.

Le créateur d'un espace en est automatiquement gestionnaire. S'il s'agit d'une espace ou d'un document, il est également gestionnaire de tout le contenu ajouté.
<br>
<br>

---
!!! check inline "Rôle"
    - [ ] Gestionnaire
    - [X] Éditeur
    - [ ] Lecteur

<br>

Un **éditeur** peut tout consulter et créer du contenu, mais ne peut pas Supprimer :fontawesome-regular-trash-can: ou changer l'emplacement du contenu qu'il n'a pas créée.

<br>
<br>

---
!!! check inline  "Rôle"
    - [ ] Gestionnaire
    - [ ] Éditeur
    - [X] Lecteur

<br>

Un **lecteur** peut uniquement consulter et Télécharger :material-tray-arrow-down: le contenu.
Il peut également générer un lien qu'il peut partager en externe. 

<br>

---

## Héritage des rôles

Un rôle est automatiquement hérité.

Quand un Espace de travail est partagé l'utilisateur hérite des mêmes droits sur les dossiers et documents qui le composent.

Tous les dossiers ou documents ajoutés après le partage profite de cet héritage.


## Action limitée par le rôle attribué à l'espace de travail (:material-briefcase-variant-outline:).
### Gestionnaire
!!! check inline "Rôle : action disponible"
    - [x] Gestionnaire
      - [ ] Collaborateur / Éditeur
      - [ ] Lecteur

- :material-briefcase-variant-outline: Espace de travail:
    - Favoris :octicons-star-16: 
    - Modifier :octicons-pencil-24:  
    - Gérer les membres :octicons-person-24:
    - Supprimer :fontawesome-regular-trash-can:
- :fontawesome-regular-folder: Dossier :
    - Modifier :octicons-pencil-24: 
    - Supprimer :fontawesome-regular-trash-can:
    - Copier :material-file-multiple-outline:
    - Déplacer :material-arrow-right-top:
- :octicons-file-24: Fichier :
    - Favoris :octicons-star-16: 
    - Obtenir un lien :fontawesome-solid-link:
    - Éditer avec Collabora :fontawesome-solid-link: (uniquement pour les formats modifiables)
    - Importer une nouvelle version :material-tray-arrow-up:
    - Afficher en PDF :octicons-eye-24: (uniquement pour les formats modifiables)
    - Modifier :octicons-pencil-24: 
    - Supprimer :fontawesome-regular-trash-can:
    - Copier :material-file-multiple-outline:
    - Déplacer :material-arrow-right-top:
    - Télécharger :material-tray-arrow-down:


### Éditeur
!!! check inline "Rôle : action disponible"
    - [ ] Gestionnaire
      - [X] Collaborateur / Éditeur
      - [ ] Lecteur

- :material-briefcase-variant-outline: Espace de travail (non crée par l'utilisateur):
    - Favoris :octicons-star-16: 
    - Modifier :fontawesome-solid-link:  (consultation uniquement)
    - Gérer les membres :octicons-person-24: (consultation uniquement)
    - Supprimer :fontawesome-regular-trash-can: (impossible)
- :fontawesome-regular-folder: Dossier (non crée par l'utilisateur):
    - Modifier :octicons-pencil-24: 
    - Supprimer :fontawesome-regular-trash-can: (impossible)
    - Copier :material-file-multiple-outline:
- :octicons-file-24: Fichier (non crée par l'utilisateur):
    - Favoris :octicons-star-16: 
    - Obtenir un lien :fontawesome-solid-link:
    - Éditer avec Collabora :fontawesome-solid-link: (uniquement pour les formats modifiables)
    - Importer une nouvelle version :material-tray-arrow-up:
    - Afficher en PDF :octicons-eye-24: (uniquement pour les formats modifiables)
    - Modifier :octicons-pencil-24: 
    - Supprimer :fontawesome-regular-trash-can: (impossible)
    - Copier :material-file-multiple-outline:
    - Télécharger :material-tray-arrow-down:





### Lecteur
!!! check inline  "Rôle : action disponible"
    - [ ] Gestionnaire
      - [ ] Collaborateur / Éditeur
      - [X] Lecteur

- :material-briefcase-variant-outline: Espace de travail (non crée par l'utilisateur):
    - Favoris :octicons-star-16: 
    - Modifier :octicons-pencil-24:  (consultation uniquement)
    - Gérer les membres :octicons-person-24: (consultation uniquement)
    - Supprimer :fontawesome-regular-trash-can: (impossible)
- :fontawesome-regular-folder: Dossier (non crée par l'utilisateur):
    - Copier :material-file-multiple-outline:
- :octicons-file-24: Fichier (non crée par l'utilisateur):
    - Favoris :octicons-star-16: 
    - Obtenir un lien :fontawesome-solid-link:
    - Afficher en PDF :octicons-eye-24: (uniquement pour les formats modifiables)
    - Copier :material-file-multiple-outline:
    - Télécharger :material-tray-arrow-down:


<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Gestion des membres](gestion-membres.md)
    - [Mes Espaces](../espaces/mes-espaces.md)
    - [Pristy Espaces](../espaces/se-reperer.md)


!!! info inline end "Pour aller plus loin :"
    - [Administrer Pristy](../portail/administration.md)

<!-- end Liens de bas de page -->

[//]: # (TODO 24-01-31 : après 'sprint fin janvier - S9' update avec les nouveautés)
