---
title: Obtenir un lien
description: Obtenir un lien vers un fichier dans Pristy
---


<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Obtenir un lien

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [X] Pristy Espaces
      - [X] Pristy Actes
      - [ ] Pristy Portail

!!! check inline end "[Rôle](roles.md) nécessaire pour ajouter/supprimer un utilisateur/groupe"
    - [x] Utilisateur Gestionnaire
    - [x] Utilisateur Éditeur
    - [x] Utilisateur Lecteur

---

!!!info "Pré-requis"
    - aucun

<br>
<br>
<!-- end Prérequis -->

## Points clefs 

Vous pouvez partager vos documents via un lien. 

Le contenu n'est accessible que pour les personnes qui ont accès au lien. 

Le lien peut avoir une date d'expiration. 

Le partage du document peut être révoqué à n'importe quel moment. 

## Obtenir le lien 

- Lorsque vous êtes dans un espace ou un dossier, repérez le document que vous voulez partager
- Cliquez sur le logo "lien" :fontawesome-solid-link: en bout de ligne 
- La pop-up "Lien à partager" s'ouvre
- Cliquer sur "Créer un lien de partage"
- Cliquer sur "Copier le lien de partage"
- Le lien est dans votre presse-papier, vous pouvez le coller dans un message

### Retrouver un lien de partage

Si un document a déjà été partagé, vous pouvez retrouver son lien :

- Cliquez sur le logo "lien" :fontawesome-solid-link: en bout de ligne
- La pop-up "Lien à partager" s'ouvre
- Cliquer sur "Copier le lien de partage"
- Le lien est dans votre presse-papier, vous pouvez le coller dans un message

### Supprimer un lien de partage

Si un document a déjà été partagé, vous pouvez supprimer son lien :

- Cliquez sur le logo "lien" en bout de ligne
- La pop-up "Lien à partager" s'ouvre
- Cliquer sur l'icône rouge :fontawesome-regular-trash-can:{ .color-bg-red .button .color-white } "Corbeille" 

Votre lien est supprimé, les personnes à qui vous l'aviez partagé n'auront plus accès au contenu.

Vous pouvez en recréer un, il faudra alors partager le nouveau lien. 

### Ajouter une date d'expiration

Après avoir [créé un lien](#obtenir-le-lien) vous pouvez lui ajouter une date d'expiration

- Cliquer sur la case à cocher " :fontawesome-regular-square: Date d'expiration"
- Un calendrier s'ouvre
- Choisissez la date de fin de partage

Le jour après cette date le lien sera automatiquement supprimé, ceux à qui vous l'avez transmis n'auront plus accès au contenu.

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Gestion des membres](../pristy/gestion-membres.md)


!!! info inline end "Pour aller plus loin :"
    - [Mes Espaces](../espaces/mes-espaces.md)


<!-- end Liens de bas de page -->
