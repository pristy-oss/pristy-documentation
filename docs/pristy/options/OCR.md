---
title: OCR automatique
description: Grâce à cette option, chacun de vos documents scannés est automatiquement transformé pour que vous puissiez *sélectionner* et *copier/coller* son contenu.
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Option OCR automatique 

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [x] Pristy Espaces
    - [x] Pristy Actes
    - [ ] Pristy Portail
    - [x] Pristy app content


!!! check inline end "[Rôle](../roles.md) nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
    - [ ] Administrateur

---
!!!info "Pré-requis"
    - Avoir souscrit à l'option

<br> 
<br> 
<!-- end Prérequis -->

Grâce à cette option, chacun de vos documents scannés est automatiquement transformé pour que vous puissiez *sélectionner* et *copier/coller* son contenu.

Automatiquement le contenu est aussi indexé à la recherche. Un mot contenu dans le document suffit à le retrouver.

!!! tip "À savoir :"
      Cette action complète prend environ une minute, si vous l'ouvrez directement après l'import la transformation n'aura pas encore eu lieu.
      Après 1 minute actualisé la page.
      Le numéro de version change est devient 1.1 quand le processus est terminé

!!! important "Note :"
      Vous pouvez importer un grand nombre de fichiers en même temps, ils seront tous traité un à un. Ne vous inquiétez pas, c'est très rapide et cela ne ralentit pas votre utilisation de Pristy !


<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Import](../../espaces/ajouter-contenu/import.md)

<!-- end Liens de bas de page -->
