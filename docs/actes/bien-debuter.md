---
title: Bien débuter
description: Comment utiliser l'application métier Pristy Publication des Actes Administratifs
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Bien débuter 

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [ ] Pristy Espaces
    - [x] Pristy Actes
    - [ ] Pristy Portail
    - [ ] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
    - [ ] Administrateur

---
!!!info "Pré-requis"
    - Être membre sur au moins un espace acte.      

<br> 
<br> 
<!-- end Prérequis -->


## Accéder au compte

Voir [Accéder à Pristy](../pristy/acces.md)

## Se repérer dans Pristy Actes

Pour savoir comment se repérer dans Pristy rendez-vous sur [cette page](../pristy/bien-debuter.md#se-reperer-dans-pristy).

### Menu à gauche

Dans le menu, en cliquant sur "Actes Administratifs", vous accédez à la liste de tous les espaces actes auxquels vous avez accès. 

Depuis le menu, vous pouvez aussi accéder à tous les espaces actes auxquels vous avez accès. 

Tout en bas du menu se trouve l'icône recherche, qui permet de rechercher des actes dans tous les espaces actes auxquels vous avez accès. 

### Barre en-tête

À la différence des [autres applications](../pristy/bien-debuter.md#se-reperer-dans-pristy), la recherche n'est pas disponible et la page profil n'est pas accessible dans la barre d'en-tête d'Actes. 

Une action s'ajoute à la roue crantée :fontawesome-solid-link: "paramètres" : "Mes espaces actes".


## Ajouter et consulter du contenu

- Cliquer sur un espace de travail vous pourrez soit ajouter des actes administratifs, soit ajouter des séances puis les délibérations et annexes liées. 

Pour en savoir plus consulter la page sur la [gestion du contenu dans Pristy Actes](imports.md)

## Gérer les membres d'un espace

Consulter la page dédiée sur la [gestion des membres](../pristy/gestion-membres.md)

## Comprendre les rôles

Consulter la page dédiée sur les [rôles](../pristy/roles.md)


<!-- Mise à jour à venir :
Visuel : avec encadré des sections et lien vers les pages liées.
Section à transférer dans la page "se repérer" lors de la création du visuel
-->



---
!!! summary inline "En lien avec cette page :"

    - [Ajouter et modifier des actes](imports.md)
    - [Page de publication / publicité](publication.md)
    - [Espace de travail](../espaces/mes-espaces.md#creer-un-espace)
