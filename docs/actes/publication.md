---
title: Publication et Publicité
description: Comment utiliser l'application métier Pristy Publication des Actes Administratifs
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Publication et publicité des Actes en ligne

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [ ] Pristy Espaces
    - [x] Pristy Actes
    - [ ] Pristy Portail
    - [ ] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
    - [ ] Administrateur

---
!!!info "Pré-requis"
      - Être dans un espace actes
      - Avoir [Importer et modifier des actes](imports.md)
<br> 
<br> 
<!-- end Prérequis -->


Aussi appelé publicité, c'est ce qui permet de rendre disponibles les actes en ligne aux administrés.

Une page avec un lien dédié est créé. Elle regroupe tous les actes d'un espace acte.

La publication des actes sur cette page se fait après avoir appuyé sur le bouton publier.

Ces documents ne sont accessibles que depuis la page dédiée, et n'apparaissent pas en résultat des moteurs de recherches.

## Publication et tampon
!!! success "Nouveauté d'octobre 2022"
      Mise à jour 0.6.0
      (*[note de version](https://gitlab.com/pristy-oss/pristy-actes/-/tags/0.6.0)*)

Pour publier un acte : cliquer sur le bouton bleu "publier".

Après quelques secondes, l'acte tamponné est publié et disponible sur la page dédié aux administrés.

Sur le document est alors affiché un tampon "Publié le" avec la date de publication.

La date d'affichage est alors indiqué, ce qui permet de contrôler le délai légal de contestation.

<!-- Mise à jour à venir : vidéo-->

??? example "En vidéo : Publier des actes"
      <figure> <video width="100%" controls>
      <source src="https://jeci.pristy.net/alfresco/api/-default-/public/alfresco/versions/1/shared-links/fcmdT-DURj2VPTeVneqEiw/content?attachment=false" type="video/webm">
      Votre navigateur ne supporte pas le tag vidéo.
      </video>
      <!--Actes-importer-publier-2-->
      <figcaption>Publier des actes</figcaption>
      </figure>

## Actes Individuels

Les actes individuels publiés ne sont pas disponibles publiquement. Si vous choisissez ce type, seul les personnes autorisées (utilisateur connecté ayant accès à l'espace) peuvent le consulter.

<!-- Mise à jour à venir :
Il est aussi possible de partager ce document avec un lien direct, à une personne extérieure.
-->

??? example "En vidéo : Page de consultation et actes individuels"
      <figure> <video width="100%" controls>
      <source src="https://jeci.pristy.net/alfresco/api/-default-/public/alfresco/versions/1/shared-links/Yj6UJx1xTeWrqGy65y_Oxw/content?attachment=false" type="video/webm">
      Votre navigateur ne supporte pas le tag vidéo.
      </video>
      <!--Page-de-consulation-ai-->
      <figcaption>Page de consultation et actes individuels</figcaption>
      </figure>

## Page de Publication

Pour obtenir le lien :

-   Être dans l'espace "Actes" approprié
-   Cliquer sur "Options"
-   Puis cliquer sur "Afficher le lien public"
-   Cliquer sur le lien indiqué dans le pop-up

^^Attention^^, la visibilité est différente en fonction du [type de visiteur](#visiteurs-non-connecte).

| Les vignettes             | Le nom du document                                     | La date de la séance où l'acte a été voté                                       | Sa date de publication dans la plateforme                                       | La nature (type) de fichier en toutes lettres                                                                                        |
|:--------------------------|:-------------------------------------------------------|:--------------------------------------------------------------------------------|:--------------------------------------------------------------------------------|:-------------------------------------------------------------------------------------------------------------------------------------|
| Première page du document | [Tri](recherche.md#tri) possible de A à Z et de Z à A) | [Tri](recherche.md#tri) possible du plus ancien au plus récent (et inversement) | [Tri](recherche.md#tri) possible du plus ancien au plus récent (et inversement) | Délibération, Actes réglementaires, Actes individuels, Contrats conventions et avenants, Documents budgétaires et financiers, Autres |

### Utilisateurs connectés (ayant accès à Pristy Actes)

Vous pouvez voir et consulter les actes individuels et les documents non publiés.
Un bandeau bleu vous en informe.

### Visiteurs (non connecté)

Si vous utilisez la navigation privée ou que vous vous déconnectez (tel un administré) la page n'affichera plus les actes individuels.

??? example "En vidéo : Page de consultation"

      <figure> <video width="100%" controls>
        <source src="https://jeci.pristy.net/alfresco/api/-default-/public/alfresco/versions/1/shared-links/jyhFRmqFRHeACZpMOASQLA/content?attachment=false" type="video/webm">
      Votre navigateur ne supporte pas le tag vidéo.
      </video>
      <!--Page-consultation-->

      <figcaption>Page de consultation</figcaption>
      </figure>

!!! important "Personnalisable"
      Cette page a été pensé pour être intégré à votre site web.

!!! info "Recherche et filtre"
      Il est possible de rechercher et de filtrer les contenu. Pour en savoir plus rendez-vous sur [Recherche, Filtre et Tri](recherche.md)
---

!!! summary inline "En lien avec cette page :"

    - [Importer et modifier des actes](imports.md)
    - [Recherche et filtre des actes](recherche.md)
