---
title: Administrer vos espaces Actes
description:
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Administrer vos espaces Actes

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [ ] Pristy Espaces
    - [x] Pristy Actes
    - [ ] Pristy Portail
    - [ ] Pristy app content


!!! check inline end "Rôle nécessaire"
    - [ ] Utilisateur (lecteur / éditeur / gestionnaire)
    - [x] Administrateur


!!!warning "Pré-requis"
    Ces actions ne sont disponibles qu'aux administrateurs désignés dans la collectivité.
      Elles permettent de gérer la création des espaces Actes et de gérer les accès des utilisateurs.
      
      Si besoin, contactez votre administrateur.

<!-- end Prérequis -->


## Créer un espace "Actes"

Sur la page mes espaces actes `https://votre-commune.pristy.net/actes/mes-espaces-actes`

Disponible dans le menu ou sur la roue crantée. 

- Cliquer sur créer un espace
- Renseigner un ID à l'espace. L'ID correspond à l'url de l'espace. Il doit respecter des règles de saisie : pas de caractère spéciaux, seuls des lettres, des chiffres et le tiret médian (-) sont acceptés.
^^Attention^^, il n'est pas possible de changer l'ID après la création.
- Ajouter si vous le souhaitez une description et valider.

Votre espace est créé. Vous pouvez modifier son nom pour qu'il soit plus lisible. L'ID restera inchangé. 

## Ajouter des utilisateurs à Pristy

Fonctionnalité à venir début 2023.

Pour en savoir plus rendez-vous sur la [page d'administration de Pristy Portail](../portail/administration.md)

## Gérer les membres d'un espace

Consulter la page dédiée sur la [gestion des membres](../pristy/gestion-membres.md)


## Configurer la page de consultation des administrés

Contactez-nous si besoin.





---
!!! summary inline "En lien avec cette page :"

    - [Ajouter et modifier des actes](imports.md)
    - [Page de publication / publicité](publication.md)
    - [Espace de travail](../espaces/mes-espaces.md#creer-un-espace)
