---
title: Présentation de Pristy Actes
description: Présentation de l'application métier Pristy Publication des Actes Administratifs
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Présentation de l'application Pristy Actes
Pristy Publication des Actes Administratifs (Pristy Actes) est une application métier à destination des Collectivités Territoriales.

Elle s'intègre dans la suite d'application de Pristy et ses nouvelles interface. Elle peut être commandée seule.

Par publication, nous entendons aussi publicité des actes administratifs.

!!! help "Donnez votre avis !"
      Cette application à pour but de respecter la [loi "engagement dans la vie locale et à la proximité de l'action publique"](https://www.legifrance.gouv.fr/loda/article_lc/LEGIARTI000039764719/). Tout commentaire est le bienvenu.

## Démonstration

!!! example "En vidéo : Démo courte"
      <figure> <video width="100%" controls>
      <source src="https://jeci.pristy.net/alfresco/api/-default-/public/alfresco/versions/1/shared-links/Eor8bmotQUG4hyQe1AmQQA/content?attachment=false" type="video/webm">
      Votre navigateur ne supporte pas le tag vidéo.
      </video>
      <!--Demo-courte-->
      <figcaption>Démo courte : ajout de délibération dans une séance</figcaption>
      </figure>
      <!--Vidéo à mettre à jour avec nouvelle interface-->



??? example "En vidéo : Démo longue"
      <figure> <video width="100%" controls>
      <source src="https://jeci.pristy.net/alfresco/api/-default-/public/alfresco/versions/1/shared-links/zs4WXtq-TOS_YLLLrtTQoQ/content?attachment=false" type="video/webm">
      Votre navigateur ne supporte pas le tag vidéo.
      </video>
      <!--Demo-longue-->
      <figcaption>Démo longue : rapide passage sur toutes les fonctionnalités proposé au 31 juillet 2022</figcaption>
      </figure>
      <!--Vidéo à mettre à jour avec nouvelle interface-->

---
!!! summary  "Sommaire"

      - [Bien débuter : accès](bien-debuter.md)
      - [Administrer les espaces](administration.md)
      - [Importer et modifier des actes](imports.md)
      - [Page de publication / publicité](publication.md)
      - [Recherche et filtre des actes](recherche.md)
---


<!-- Liens de bas de page -->

!!! summary inline "En lien avec cette page :"
      - [Presentation Pristy](presentation.md)
      - [Presentation Pristy Espaces](../espaces/presentation.md)
      - [Presentation Pristy Portail](../portail/presentation.md)
