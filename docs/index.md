---
title: Documentation Pristy
description: Vous trouverez ici toute la documentation, manuel utilisateur, pour l'utilisation de Pristy. Vous êtes débutants ? On part du début. Vous êtes avancé, il y a des conseils qui peuvent vous être utiles.
hide:
  - footer
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Documentation de Pristy et ses applications

Bienvenue sur la documentation de Pristy.
Elle va évoluer, s'enrichir et se clarifier en même temps que Pristy grandit. Si vous avez des questions ou des suggestions d'amélioration n'hésitez pas à prendre contact avec nous ou à ouvrir un ticket sur le [gitlab de la documentation](https://gitlab.com/pristy-oss/pristy-documentation).

## Qu'est-ce que Pristy ?
Pristy est une suite d'application GED métier. 
Pristy Espaces, notre GED complète intègre la suite bureautique Collabora Online à la GED (gestion électronique de documents) Alfresco et crée une alternative française et libre à Microsoft 365 et Google Drive. Vos documents sont accessibles depuis votre navigateur dans une nouvelle interface simple d'utilisation et accessible à tous.

*[GED]: Gestion Électronique de Documents

## Que vais-je trouver dans la documentation ?

Vous trouverez dans cette documentation une description de toutes les fonctionnalités de Pristy et des conseils d'utilisation.
Si des informations vous manquent, ou que cela manque de précision n'hésitez pas à écrire à [Lucie](mailto:lucie@jeci.fr) ou à ouvrir un ticket sur le [gitlab de la documentation](https://gitlab.com/pristy-oss/pristy-documentation).

## Où acheter Pristy ?

Pour acheter Pristy rendez-vous sur [pristy.fr](https://pristy.fr/fr/) ou contactez nous pour obtenir un devis.

Vous pouvez aussi [essayer Pristy(https://pristy.fr/fr/demo) ou nous demander une démo personnalisé, pour cela prenez contact avec nous [pristy.fr/fr/contact](https://pristy.fr/fr/contact)

## Est-ce que je peux participer à l'écriture de la documentation ?

Bien sûr ! Cette documentation, comme [Pristy](https://gitlab.com/pristy-oss), est [libre](https://pristy.fr/fr/logiciels-libres/).
Pour participer, rendez-vous sur le [projet GitLab](https://gitlab.com/pristy-oss/pristy-documentation).


### [Essayer Pristy :material-account-plus:](https://pristy.fr/fr/contact){ .md-button .md-button--primary }
