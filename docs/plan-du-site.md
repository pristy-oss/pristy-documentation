---
title: Plan du site
description: Contenu du site de la documentation de Pristy et ses applications.
hide:
  - footer
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Plan du site

- [Accueil](index.md)
* [Sommaire](plan-du-site.md)

  * Pristy Général
    * [Présentation](pristy/presentation.md)
    * [Accéder au compte](pristy/acces.md)
    * [Bien débuter dans Pristy](pristy/bien-debuter.md)
    * [Gestion des membres](pristy/gestion-membres.md)
    * [Rôles dans Pristy](pristy/roles.md)
    * Options :
        * [OCR](pristy/options/OCR.md)
  * Pristy Espaces
    * [Présentation](espaces/presentation.md)
    * [Bien débuter](espaces/bien-debuter.md)
    * [Espaces de travail](espaces/mes-espaces.md)
    * [Se repérer dans un espace / dossier⚓︎](espaces/se-reperer.md)
    * Ajouter du contenu : 
      * [Création de contenu](espaces/ajouter-contenu/creer.md)
      * [Importer des éléments](espaces/ajouter-contenu/import.md)
    * Gérer du contenu : 
      * [Édition en ligne](espaces/gerer-contenu/edition-en-ligne.md)
      * [Copier / Déplacer](espaces/gerer-contenu/copier-deplacer.md)
      * [Ajouter / Supprimer des favoris](espaces/gerer-contenu/favoris.md)
      * [Utiliser l’action modifier](espaces/gerer-contenu/modifier.md)
      * [Télécharger & imprimer un élément](espaces/gerer-contenu/telecharger.md)
      * [Supprimer un élément](espaces/gerer-contenu/supprimer.md)
    * Zone d'information :
        * [Détails et métadonnées](espaces/zone-information/details.md)
        * [Gestion de versions d'un élément (nouveauté !)](espaces/zone-information/gestion-versions.md)
    * [Formats pris en charge](espaces/formats-pris-en-charge.md)
  * Pristy Portail
    * [Présentation](portail/presentation.md)
    * [Bien débuter](portail/bien-debuter.md)
    * [Administrer Pristy](portail/administration.md)
    * [Page Profil](portail/page-profil.md)
    * [Corbeille](portail/corbeille.md)
  * Pristy Actes Administratifs
    * [Présentation](actes/presentation.md)
    * [Bien débuter](actes/bien-debuter.md)
    * [Administrer vos espaces Actes](actes/administration.md)
    * [Ajouter actes et annexes](actes/imports.md)
    * [Publication et publicité](actes/publication.md)
    * [Recherche, Filtre et Tri](actes/recherche.md)
  * Installer Pristy
    * [Installer Pristy Démo](installation/installer-pristy.md)
    * [Installer Pristy sur Alfresco](installation/installer-pristy-sur-alfresco.md)
  * [Pristy.fr](https://pristy.fr)
  * [Contact](https://pristy.fr/fr/contact)
  * Release Notes
    *  [23.1.1](release-notes/23-1-1.md)
<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    * [Présentation Pristy Portail](pristy/presentation.md)
      * [Se repérer dans Pristy](pristy/bien-debuter.md#se-reperer-dans-pristy)
      * [Se repérer dans Pristy Espaces](espaces/bien-debuter.md#se-reperer-dans-pristy-espaces)
      * [Se repérer dans Pristy Actes](actes/bien-debuter.md#se-reperer-dans-pristy-actes)

!!! info inline end "Pour aller plus loin :"
    * [Page profil](portail/page-profil.md)
      * [Corbeille](portail/corbeille.md)

<!-- end Liens de bas de page -->
