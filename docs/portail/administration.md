---
title: Administrer Pristy
description:
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Administrer vos instances de Pristy

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [ ] Pristy Espaces
      - [x] Pristy Actes
      - [ ] Pristy Portail
      - [ ] Pristy app content


!!! check inline end "Rôle nécessaire"
    - [ ] Utilisateur (lecteur / éditeur / gestionnaire)
      - [x] Administrateur


!!!warning "Pré-requis"
    Ces actions ne sont disponibles qu'aux administrateurs désignés dans la collectivité.
    Elles permettent de gérer la création des espaces Actes et de gérer les accès des utilisateurs.
    
    Si besoin, contactez votre administrateur.

<!-- end Prérequis -->

## Ajouter des utilisateurs

Fonctionnalité à venir début 2023.

En attendant pour ajouter, supprimer et modifier des utilisateurs, contacter votre référent dans notre équipe ou écrivez au support.

<!-- Mise à jour à venir :
Page de gestion des utilisateurs, disponible sur la roue crantée
-->

