---
title: Page profil
description: Page profil - Pristy Portail
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Page Profil (:octicons-person-24:)

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [ ] Pristy Espaces
      - [ ] Pristy Actes
      - [x] Pristy Portail
      - [ ] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
      - [ ] Administrateur

---
!!!info "Pré-requis"
    - Aucun

<br> 
<br> 
<!-- end Prérequis -->

## Consulter ses données personnelles

Sur la page profil, vous trouvez votre nom, prénom et adresse e-mail renseigné par l'administrateur lors de la création de votre compte.

Pour agir sur vos données, veuillez contacter votre administrateurs

## Choix de la langue

Sur votre page profil vous pouvez sélectionner la langue de l'application : français ou anglais. 

Cliquer sur le switch pour choisir votre langue. 

Besoin d'un autre langue ? [Contactez-nous](https://pristy.fr/fr/contact) !