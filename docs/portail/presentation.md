---
title: Présentation de Pristy Portail
description: Présentation de l'application Pristy Portail
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Présentation de l'application Pristy Portail
Pristy Portail est une application qui veut proposer un portail aux applications Pristy et simplifier la gestion des administrateurs.

Le but de Pristy : rester compatible à Alfresco tout en proposant des interfaces modernes.

Cette application est encore en version alpha, la documentation va donc évoluer en parallèle de son développement.

À venir rapidement : page de "Gestion Utilisateurs", page de "Gestion de profil et donnée", etc

Pour suivre l'avancée de l'application rendez-vous sur [notre roadmap](https://pristy.fr/fr/roadmap/).

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Presentation Pristy](presentation.md)
      - [Presentation Pristy Espaces](../espaces/presentation.md)
      - [Presentation Pristy Actes](../actes/presentation.md)
