---
title: Bien débuter
description: Bien débuter sur Pristy Portail
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

[//]: # (TODO 2023 - Créer visuel : avec encadré des sections et lien vers les pages liées.)

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [ ] Pristy Espaces
      - [ ] Pristy Actes
      - [x] Pristy Portail
      - [ ] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
      - [ ] Administrateur

---
!!!info "Pré-requis"
    - Aucun

<br> 
<br> 
<!-- end Prérequis -->

## Accéder au compte

Voir [Accéder à Pristy](../pristy/acces.md)

## Se repérer dans Pristy Portail

Pour savoir comment se repérer dans Pristy rendez-vous sur [cette page](../pristy/bien-debuter.md#se-reperer-dans-pristy).

### Menu à gauche

Le menu à gauche se divise en différentes sections non cliquables : 
- Portail Pristy
- Portail administrateur
- Mes Espaces (si vous avez accès à cette application)
- Actes Administratifs (si vous avez accès à cette application)

#### Portail Pristy
!!! check inline end "Rôle nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
      - [ ] Administrateur

Cette section est accessible à tous les utilisateurs, il permet de gérer ses données personnelles. 

Elle permet d'accéder à :

- sa [page profil](page-profil.md)
- sa [corbeille](corbeille.md) pour pouvoir supprimer ou restaurer le contenu supprimer depuis n'importe quelle application

#### Portail Administrateur
!!! check inline end "Rôle nécessaire"
      - [ ] Utilisateur (lecteur / éditeur / gestionnaire)
      - [x] Administrateur

Cette section est accessible aux administrateurs de votre équipe dans Pristy.

Elle permet de gérer les membres en accédant à la page de gestion des utilisateurs.

!!! warning "Note : à venir"
    Les pages Factures et Statistiques ne sont pas encore développées, les liens ne sont donc pas actifs.

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    * [Presentation Pristy Portail](presentation.md)
    * [Se repérer dans Pristy](../pristy/bien-debuter.md#se-reperer-dans-pristy)
    * [Se repérer dans Pristy Espaces](../espaces/bien-debuter.md#se-reperer-dans-pristy-espaces)
    * [Se repérer dans Pristy Actes](../actes/bien-debuter.md#se-reperer-dans-pristy-actes)

!!! info inline end "Pour aller plus loin :"
    * [Page profil](page-profil.md)
    * [Corbeille](corbeille.md)

<!-- end Liens de bas de page -->
