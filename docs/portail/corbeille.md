---
title: "Corbeille"
description: Supprimer définitivement et restaurer des éléments présents dans la corbeille
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Corbeille (:fontawesome-regular-trash-can:) : Supprimer / Restaurer des éléments

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [ ] Pristy Espaces
      - [ ] Pristy Actes
      - [x] Pristy Portail
      - [ ] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
      - [ ] Administrateur

---
!!!info "Pré-requis"
    Cette étape suppose que vous vous êtes connecté à Pristy et avez accès à au moins un espace de travail contenant des dossiers et/ou fichiers et que vous avez supprimé au moins un élément.
    Si ce n'est pas le cas vous [créer un espace de travail]() et [ajouter des dossiers]() ainsi qu'[importer vos fichiers](). Et enfin, [supprimer un élément]().

<br> 
<br> 
<!-- end Prérequis -->

Vous pouvez accéder à la corbeille de Pristy depuis n'importe quelle application en cliquant sur la roue crantée :octicons-gear-16: "Paramètres" puis sur ":fontawesome-regular-trash-can: Corbeille"

## Supprimer

### Un élément 
Pour supprimer définitivement un élément de la corbeille cliquer sur le bouton rouge ":fontawesome-regular-trash-can:{ .color-bg-red .button .color-white } Corbeille" sur la fin de la ligne de l'élément. 

### Plusieurs éléments

Ce n'est aujourd'hui pas possible.

### Vider la corbeille

Cliquer sur le bouton tout supprimer et valider la pop-up de confirmation qui vous informera du nombre d'éléments à supprimer. 

!!! warning "Attention !"
    Attention cette action est définitive.

## Restaurer

### Un élément

Pour restaurer définitivement un élément de la corbeille cliquer sur le bouton gris ":material-history:{ .color-bg-grey .button .color-white } Restaurer" sur la fin de la ligne de l'élément.

L'élément sera restauré à son emplacement d'origine.

!!! tip "Trouver l'élément restaurer"
    Avant de restaurer l'élément, copier son nom, après restauration utiliser coller son nom dans la recherche pour le retrouver. 

### Plusieurs éléments

Ce n'est aujourd'hui pas possible. 


<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Bien débuter](bien-debuter.md)
