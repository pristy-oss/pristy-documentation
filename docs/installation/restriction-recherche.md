---
title: Restriction de la recherche utilisateur
description: Restreindre la recherche d'utilisateurs pour les membres.
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [x] Communautaire
    - [ ] Pristy sans option
    - [x] Option disponible


!!! check inline end "Rôle nécessaire"
    - [ ] Utilisateur (lecteur / éditeur / gestionnaire)
    - [x] Administrateur

---
!!!info "Pré-requis"
    - Aucun

<br>
<br>
<!-- end Prérequis -->

Restreindre la recherche d'utilisateurs pour les membres.

### Fonctionnement

!!! question "En quoi ça consiste"
    Lors d'une recherche utilisateur, la liste des utilisateurs retournés est filtrée en fonction :

    - du nom de domaine de l'adresse mail de l'utilisateur effectuant la recherche
    - du nom de domaine de l'adresse mail de l'utilisateur recherché
    - des zones[^1] auxquelles appartient l'utilisateur recherché

Cas d'exclusion de la liste retournée :

- l'adresse mail de l'utilisateur recherché n'a pas le même nom de domaine que celui de l'adresse mail de l'utilisateur effectuant la recherchant
- l'adresse mail de l'utilisateur recherché a un nom de domaine défini dans la liste à ignorer
- l'utilisateur recherché appartient à une zone[^1] définie dans liste à ignorer

!!! info "Cas particulier"
    - Si l'utilisateur effectuant la recherche n'a pas d'adresse mail, sa recherche retournera toujours aucun résultat
    - Si un utilisateur n'a pas d'adresse mail défini, il ne remontera dans aucune recherche

[^1]: élément technique défini dans alfresco

### Activation

La recherche utilisateur peut être restreinte en activant cette option `search-user-restriction.enable=true`.

!!! warning "Attention"
    La restriction est une surcharge du webservice `/alfresco/api/-default-/public/alfresco/versions/1/people`,
    elle sera donc appliquée sur toutes les interfaces qui font appel à ce webservice.

### Configuration

Les différents critères de restriction peuvent être définis à partir des propriétés suivantes :

- `search-user-restriction.hidden_domains` : liste des domaines non affichés
- `search-user-restriction.hidden_zones`: liste des zones non affichées

!!! info "Valeurs par défaut"
    - `search-user-restriction.hidden_domains=gmail.com,wanadoo.fr,hotmail.com,hotmail.fr,aol.com,outlook.com,outlook.fr,free.fr,orange.fr,yahoo.fr,yahoo.com,msn.com,neuf.fr,cegetel.net,icloud.com,laposte.net,microsoft.com,sfr.fr,numericable.fr`
    - `search-user-restriction.hidden_zones=APP.ACTES`

Si vous souhaitez que certains utilisateurs ne soient pas impactés par cette restriction, il faut les ajouter dans le groupe `GROUP_FIND_USERS`.


<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Installer Pristy sur Alfresco](installer-pristy-sur-alfresco.md)

!!! info inline end "Pour aller plus loin :"
    - [Installer Pristy](installer-pristy.md)

<!-- end Liens de bas de page -->
