---
title: Installer Pristy sur Alfresco
description: Documentation pour vous accompagner dans l'installation de Pristy sur Alfresco.
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [x] Communautaire
    - [ ] Pristy sans option
    - [x] Option disponible


!!! check inline end "Rôle nécessaire"
    - [ ] Utilisateur (lecteur / éditeur / gestionnaire)
    - [x] Administrateur

---
!!!info "Pré-requis"
    - Aucun

<br>
<br>
<!-- end Prérequis -->

## Préparation


* [Pristy Core - Module ACS](https://gitlab.com/pristy-oss/pristy-core)
* [Pristy Espaces](https://gitlab.com/pristy-oss/pristy-espaces)
* [Pristy Portail](https://gitlab.com/pristy-oss/pristy-portail)

## Installation du module `pristy-core-platform`

* Arrêt du service

```shell
systemctl stop alfresco
```

* Ajout du module dans le dossier d'installation de vos modules `$TOMCAT_HOME/amps`

```shell
mv $DOWNLOAD_DIRECTORY/pristy-core-platform-xxx.amp $TOMCAT_HOME/amps/
```

* Déploiement des modules présents dans le dossier `amps`

```shell
java -jar $TOMCAT_HOME/alfresco-mmt/alfresco-mmt-6.0.jar \
      install $TOMCAT_HOME/amps/ $TOMCAT_HOME/webapps/alfresco \
      -nobackup -directory \
```

* Redémarrer `alfresco` pour la prise en compte de l'installation du module


## Mise en place des interfaces Pristy

Si votre serveur ne comporte pas de reverse proxy devant alfresco, vous pouvez installer les applications directement
en tant que `webapps` dans tomcat

### pristy-portail

```shell
mkdir -p $TOMCAT_HOME/webapps/portail
cd $TOMCAT_HOME/webapps/portail
tar xzf $DOWNLOAD_DIRECTORY/pristy-portail-xxx.tgz package/dist --strip-components=2
chown -R alfresco:alfresco $TOMCAT_HOME/webapps/portail
```

### pristy-espaces

```shell
mkdir -p $TOMCAT_HOME/webapps/espaces
cd $TOMCAT_HOME/webapps/espaces
tar xzf $DOWNLOAD_DIRECTORY/pristy-espaces-xxx.tgz package/dist --strip-components=2
chown -R alfresco:alfresco $TOMCAT_HOME/webapps/espaces
```

!!! warning "Utilisateur pour `chown` sur pristy-portail et pristy-espaces"
    Penser à bien indiquer l'utilisateur démarrant le processus `tomcat`.

### Configuration

* Modification du fichier `$TOMCAT_HOME/webapps/portail/env-config.json` comme suit :

```json
{
  "APP_ROOT": "/portail/",
  "ALFRESCO_HOST": "http://localhost:8080",
  "ESPACES_HOST": "http://localhost:8080",
  "ALFRESCO_AUTH": "BASIC",
  "ALFRESCO_ROOT": "alfresco",
}
```

* Modification du fichier `$TOMCAT_HOME/webapps/espaces/env-config.json` comme suit :

```json
{
    "APP_ROOT": "/espaces/"
    "ALFRESCO_HOST": "http://localhost:8080",
    "PORTAIL_HOST": "http://localhost:8080",
    "ALFRESCO_AUTH": "BASIC",
    "ALFRESCO_ROOT": "alfresco",
    "BREADCRUMB_ROOT_URL": "/mes-espaces",
    "EDIT_WITH_MSOFFICE": true,   # permet d'activer ou non l'édition avec MS Office
    "METADATA": []                # vous pouvez définir une liste de méta-données spécifiques à afficher en plus des méta-donénes standard
}
```

!!! note "Url `ALFRESCO_HOST`, `PORTAIL_HOST`, `ESPACES_HOST`"
    Les 3 urls seront identiques à partir du moment où ils sont tous installés derrière la même application.
    Il faut alors indiquer le FQDN d'accès à tomcat.

#### Ajout de méta-données

Pour ajouter des méta-données spécifiques à afficher, vous devez suivre le schéma suivant :

```json
"METADATA": [
    {
      "id": "am:dateSeance",
      "labelId": "am_date_seance",
      "display": ["datatable", "popup"],
      "input": "date",
      "readOnly": true,
      "modelAspect": "",
      "modelType": "am:documentActe",
      "order": 1
    },
    {
      "id": "am:codeActe",
      "labelId": "am_code_acte",
      "display": ["datatable", "popup"],
      "input": "text",
      "readOnly": false,
      "modelAspect": "",
      "modelType": "am:documentActe",
      "order": 2
    }
```

## Recette

Vous pouvez accéder aux applications suivantes :

* pristy-portail : http[s]://`serveur_tomcat`:`port_tomcat`/portail/
* pristy-espaces : http[s]://`serveur_tomcat`:`port_tomcat`/espaces/






<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Installer Pristy](installer-pristy.md)

!!! info inline end "Pour aller plus loin :"
    - [Restreindre la recherche d'utilisateurs](restriction-recherche.md)

<!-- end Liens de bas de page -->
