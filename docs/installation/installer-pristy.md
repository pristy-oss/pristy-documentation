---
title: Installer Pristy
description: Pour essayer pristy, rendez-vous sur notre gitlab/pristy-oss
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->
<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [x] Communautaire
    - [ ] Pristy sans option
    - [x] Option disponible


!!! check inline end "Rôle nécessaire"
    - [ ] Utilisateur (lecteur / éditeur / gestionnaire)
    - [x] Administrateur

---
!!!info "Pré-requis"
    - Aucun

<br>
<br>
<!-- end Prérequis -->

Pristy est sous [licence GNU AGPL3](https://gitlab.com/pristy-oss/pristy-demo/-/blob/stable/LICENSE).

Pour essayer Pristy, plusieurs solutions : 

- Demandez un accès à notre plateforme en ligne : [Nous contacter](https://pristy.fr/fr/contact/)
- Utilisez notre stack docker compose : [projet Pristy Demo sur GitLab](https://gitlab.com/pristy-oss/pristy-demo)
- Vous avez déjà Alfresco ? Suivez [la procédure manuelle](installer-pristy-sur-alfresco.md).
- Vous êtes développeurs ? Le code source des projets est sur gitlab
    - https://gitlab.com/pristy-oss


Profitez-en pour ajouter votre brique à l'édifice en participant au développement de Pristy !



<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Installer Pristy sur Alfresco](installer-pristy-sur-alfresco.md)

!!! info inline end "Pour aller plus loin :"
    - [Restreindre la recherche d'utilisateurs](restriction-recherche.md)

<!-- end Liens de bas de page -->
