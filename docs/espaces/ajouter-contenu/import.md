---
title: Importer des éléments
description: Créer et importer du contenu dans Pristy : créer un espace de travail et son contenu.
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Importer des éléments

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [ ] Pristy Espaces
    - [ ] Pristy Actes
    - [ ] Pristy Portail
    - [x] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
    - [ ] Administrateur
---
!!! info "Pré-requis"
        - Avoir un espace de travail avec droit d'édition (pour la gestion des espaces de travail, rendez-vous sur la page dédiée aux [espaces de travail](../mes-espaces).md)

<br> 
<br> 
<!-- end Prérequis -->


## Importer du contenu

!!! info "Pré-requis"
        - Être dans un espace de travail avec droits d'éditions

### Importer des fichiers

Vous pouvez déposer des fichiers dans un espace ou un dossier grâce au glisser déposer.

- Cliquez sur le bouton vert "Importer" (en haut à droite)
- Cliquez sur "Sélectionner des fichiers", une fenêtre vous permet de choisir vos documents
- OU Glisser des documents dans la box prévue à cet effet
- Cliquez sur ":material-tray-arrow-up: Envoyer"

Vos fichiers sont importés dans le dossier où vous vous situez.
Vous pouvez sélectionner plusieurs fichiers et ils seront tous importés.

### Format compatible

Vous pouvez ajouter des fichiers de tous types pour les stocker.
Pour la consultation et l'édition en ligne [Formats pris en charge](../formats-pris-en-charge.md) sont nombreux.

### Importer un dossier

Cette fonctionnalité sera bientôt dans Pristy Espaces !

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Créer](creer.md)
    - [Zone d'information : détails](../zone-information/details.md)
    - [Modifier](../gerer-contenu/modifier.md)


!!! info inline end "Pour aller plus loin :"
    - [Comprendre les espaces de travail](../mes-espaces.md)
    - [Zone d'information : versions](../zone-information/gestion-versions.md)
    - [Favoris](../gerer-contenu/favoris.md)

<!-- end Liens de bas de page -->
