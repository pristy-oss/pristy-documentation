---
title: Création de contenu
description: Créer et importer du contenu dans Pristy : créer un espace de travail et son contenu.
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Création de contenu

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [ ] Pristy Espaces
    - [ ] Pristy Actes
    - [ ] Pristy Portail
    - [x] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
    - [ ] Administrateur
---
!!! info "Pré-requis"
    * Avoir un espace de travail avec droit d'édition (pour la gestion des espaces de travail, rendez-vous sur la page dédiée aux [espaces de travail](../mes-espaces.md)


<br> 
<br> 
<!-- end Prérequis -->


## Créer du contenu

!!! info "Pré-requis"
      - Être dans un espace de travail avec droits d'éditions

<!-- end Prérequis -->

### Créer un dossier

- Cliquez sur le bouton vert "Créer" (en haut à droite)
- Vous êtes sur l'onglet "Créer un dossier"
- Donner un nom à votre dossier
- Écrire une description à votre dossier (facultatif).

### Créer un fichier :
- Cliquez sur le bouton vert "Créer"
- Cliquez sur l'onglet "Créer un fichier vide"
- Donner un nom à votre fichier
- Écrire une description à votre dossier (facultatif).
- Choisir le format de fichier souhaité :
    - Pour créer fichier texte (odt) choisir "Texte"
    - Pour créer tableur (ods) choisir "Tableur"
    - Pour créer une présentation (odp) choisir "Présentation"
    - Pour créer un dessin (odg) choisir "Dessin"

### Créer un fichier à partir d'un modèle préconçu :

Cette fonctionnalité sera bientôt dans Pristy Espaces !

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    * [Importer](import.md)
    * [Zone d'information : détails](../zone-information/details.md)
    * [Modifier](../gerer-contenu/modifier.md)


!!! info inline end "Pour aller plus loin :"
    * [Comprendre les espaces de travail](../mes-espaces.md)
    * [Zone d'information : versions](../zone-information/gestion-versions.md)
    * [Favoris](../gerer-contenu/favoris.md)

<!-- end Liens de bas de page -->
