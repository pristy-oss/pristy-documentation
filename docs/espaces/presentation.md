---
title: Présentation de Pristy Espaces
description: Présentation de l'application Pristy Espaces
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Présentation de l'application Pristy Espaces

Pristy Espaces est un outil de gestion documentaire (GED), avec son interface moderne et simple d'utilisation, gérer ses documents facile et agréable.

Elle inclut les fonctionnalités les plus évidentes comme le supprimer ou le copier/déplacer, mais aussi les plus attendus comme la gestion de version et le traitement OCR des documents scannés. 

Cette application est encore en version stable, sa documentation évolue en parallèle de son développement.

Pristy Espaces est une application qui propose une alternative aux interfaces Alfresco.

Le but de Pristy : rester compatible à Alfresco tout en proposant des interfaces modernes.

Pour suivre l'avancée de l'application rendez-vous sur [notre roadmap](https://pristy.fr/fr/roadmap/)

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Presentation Pristy](../pristy/presentation.md)
    - [Presentation Pristy Portail](../portail/presentation.md)
    - [Presentation Pristy Actes](../actes/presentation.md)

<!-- end Liens de bas de page -->
