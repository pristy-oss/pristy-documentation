---
title: Bien débuter
description: Bien débuter sur Pristy Espaces
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

[//]: # (TODO 2023 - Créer visuel : avec encadré des sections et lien vers les pages liées.)

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [x] Pristy Espaces
    - [ ] Pristy Actes
    - [ ] Pristy Portail

!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
    - [ ] Administrateur

---
!!!info "Pré-requis"
    - Aucun

<br> 
<br> 
<!-- end Prérequis -->

## Accéder au compte

Voir [Accéder à Pristy](../pristy/acces.md)


## Se repérer dans Pristy Espaces

Pour savoir comment se repérer dans Pristy rendez-vous sur [cette page](../pristy/bien-debuter.md#se-reperer-dans-pristy).

### Menu à gauche

Dans le menu, en cliquant sur "Espaces de Travail", vous accédez à la liste de tous les espaces actes auxquels vous avez accès.

Votre menu est personnalisable grâce à vos [favoris](gerer-contenu/favoris.md).

Dans la section "[Espaces de Travail](mes-espaces.md)" du menu, vous trouverez vos **espaces de travail favoris**.

Dans la section "Favoris" (non-cliquable) du menu, vous trouverez vos **documents et dossiers favoris**.

Les documents sont représenté par une icône de documents. Les fichiers par une icône de dossier. 


## Ajouter et consulter du contenu

Entrez dans un espace pour commencer à consulter et créer du contenu. 

## Gérer les membres d'un espace

Consulter la page dédiée sur la [gestion des membres](../pristy/gestion-membres.md).

## Comprendre les rôles

Consulter la page dédiée sur les [rôles](../pristy/roles.md).


<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    * [Presentation Pristy Espaces](presentation.md)
    * [Se repérer dans Pristy](../pristy/bien-debuter.md#se-reperer-dans-pristy)
    * [Se repérer dans un espace/dossier](se-reperer.md)

!!! info inline end "Pour aller plus loin :"
    * [Mes Espaces](mes-espaces.md)
    * [Favoris](gerer-contenu/favoris.md)

<!-- end Liens de bas de page -->
