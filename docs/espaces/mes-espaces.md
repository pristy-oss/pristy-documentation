---
title: Comprendre les Espaces de travail
description: Se repérer sur la page listant les espaces de travail. 
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

[//]: # (TODO 2023 - Créer visuel : avec encadré des sections et lien vers les pages liées.)

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [x] Pristy Espaces
    - [ ] Pristy Actes
    - [ ] Pristy Portail


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
    - [ ] Administrateur

---
!!!info "Pré-requis"
    - Aucun

<br> 
<br> 
<!-- end Prérequis -->

## Qu'est-ce qu'un espace de travail

!!! tips "Métaphore pour mieux comprendre"
    Imaginez Pristy comme une bibliothèque (le lieu), chaque espace de travail est une bibliothèque (le meuble), chaque dossier est un rayon de bibliothèque et chaque fichier est un livre.

Un espace de travail est un dossier maître, il permet de simplifier l'organisation de vos documents.
Un espace de travail serait comme une section thématique dans une bibliothèque.
Lorsque vous vous connectez, en cliquant dans l'application Portail sur `Mes Espaces` vous arrivez automatiquement dans la section "Espace de travail".

Lors de votre première connexion, soit votre compte sera vide, soit des "Espaces de travail" vous auront déjà été partagés.

Il est impossible de créer de dossier ou de fichier dans cette section ; seulement des espaces de travail (comme on ne range pas un livre dans n'importe quelle section d'une bibliothèque).


## Se repérer dans la page "Mes espaces de travail"

La page "Mes espaces de travail" `ma-structure.pristy.net/espaces/mes-espaces`
se compose de différents blocks, ou boites qui représentent chacune un espace de travail.

### Composition d'une boite

Dans la boite d'un espace vous trouverez :

- en haut à gauche le nom de l'espace
- sous le nom, vous pouvez trouver la description de l'espace (facultatif)
- en haut à droite deux icônes :
    - l'étoile :octicons-star-16: pour [ajouter ou retirer un espace des favoris](gerer-contenu/favoris.md)
    - les trois points verticaux :material-dots-vertical: qui [affiche un menu déroulant d'action, différente en fonction de son rôle](../pristy/roles.md#action-limitee-par-le-role-attribue-a-lespace-de-travail) 
- en bas à gauche se trouve un verrou :material-lock-open-variant-outline: :material-lock-open-outline: :material-lock-outline: , il représente la [visibilité des espaces de travail](#visibilite-acces-a-lespace-de-travail)
  - en bas à droite est indiqué par une étiquette le [rôles](../pristy/roles.md) que l'on a sur l'espace `Lecteur` - Vert ; `Éditeur` - Jaune ; `Gestionnaire` - Bleu       

## Gérer les espaces de travail

### Créer un espace

!!! check inline end "[Rôle](../pristy/roles.md) nécessaire"
    - [x] Utilisateur Gestionnaire
    - [ ] Utilisateur Éditeur
    - [ ] Utilisateur Lecteur

Pour créer un espace de travail :

- Cliquez sur le bouton vert "Créer un nouvel espace" en haut à droite.
- Donner un ID à votre espace
  *(L’ID correspond à l’url de l’espace. Il doit respecter des règles de saisie : pas de caractère spéciaux, seuls des lettres, des chiffres et le tiret médian (-) sont acceptés. Attention, il n’est pas possible de changer l’ID après la création.)*.
- Écrire une description à votre dossier (facultatif).
    - Par exemple, "Information sur la structure" pour un dossier avec des informations générales sur l'entreprise, "Documents sur la gestion des employés" pour un dossier Ressources Humaines, "Documents relatifs à la communication" pour un dossier Communication, etc.
- Choisir le niveau de [visibilité des espaces de travail](#visibilite-acces-a-lespace-de-travail) de votre espace de travail : privé, modéré ou public.
- Votre espace est créé. Vous pouvez modifier son nom pour qu'il soit plus lisible. L'ID restera inchangé. (facultatif)


!!! top "ID et NOM tout comprendre"
    Un ID est l'identifiant unique de l'espace de travail, il sera affiché dans l'URL. Il n'est pas possible de modifier l'ID.
    Le nom d'un espace de travail est un nom d'affichage, il permet de donner plus de lisibilité aux utilisateur. 
    Lors de la création de l'espace, son nom sera son ID, il est conseillé de le modifier pour s'affranchir des caractères interdit dans l'ID.

### Modifier un espace 

!!! check inline end "[Rôle](../pristy/roles.md) nécessaire"
    - [x] Utilisateur Gestionnaire
    - [ ] Utilisateur Éditeur
    - [ ] Utilisateur Lecteur

Pour modifier un espace de travail, il faut en être éditeur ou gestionnaire.

- Cliquez sur les trois points verticaux :material-dots-vertical: à droite de l'étoile favoris :octicons-star-16:{ .color-green}
- Cliquez sur ":octicons-pencil-24: Modifier"
- Vous pouvez modifier son nom, sa description
- Si vous êtes gestionnaire vous pouvez aussi modifier le [niveau d'accès](#visibilite-acces-a-lespace-de-travail)


### Consulter un espace

!!! check inline end "[Rôle](../pristy/roles.md) nécessaire"
    - [x] Utilisateur Gestionnaire
    - [x] Utilisateur Éditeur
    - [x] Utilisateur Lecteur


Dans la section vos "Espaces de travail", vous trouverez tous les espaces de travail que vous avez créés et que l'on vous a partagé.

Cliquez simplement dessus pour voir son contenu.

Si vous en avez les [droits](../pristy/roles.md), vous pouvez modifier ces informations : 

Cliquer sur les trois points verticaux :material-dots-vertical: puis sur ":material-tray-arrow-up: Modifier"

### Quitter un espace de travail

Si vous êtes éditeur ou lecteur pour quitter un espace de travail vous devez demander au gestionnaire de vous retirer.

Si vous êtes le gestionnaire, vous pouvez donner le rôle de gestionnaire à un autre membre puis vous supprimer en tant que membre.


### Supprimer un espace de travail

!!! check inline end "[Rôle](../pristy/roles.md) nécessaire"
    - [x] Utilisateur Gestionnaire
    - [ ] Utilisateur Éditeur
    - [ ] Utilisateur Lecteur

Pour supprimer un espace de travail, il faut en être créateur ou gestionnaire.

Attention, un espace supprimé signifie que son contenu sera supprimé pour **tous les utilisateurs**. Si vous souhaitez simplement que l'espace ne fasse plus partie de votre contenu choisissez plutôt de [Quitter l'espace de travail](#quitter-un-espace-de-travail)

- Cliquez sur les trois points verticaux :material-dots-vertical: à droite de l'étoile favoris :octicons-star-16:{ .color-green}
- Cliquez sur ":fontawesome-regular-trash-can: Supprimer"

!!! summary "À savoir :"
        - Si vous ne vouliez pas supprimer cet espace : pas de panique, il est encore dans votre [corbeille](../portail/corbeille.md).
        - Si vous voulez créer un nouvel espace portant le même nom, vous devrez au préalable vider la [corbeille](../portail/corbeille.md). :warning: Attention cette action est définitive.


## Visibilité & accès à l'espace de travail

!!! note inline end "Recherche"
        Les éléments de tous les espaces publics apparaissent dans la recherche. Les éléments des espaces modérés et privés sont exclus de la recherche.

Un espace **`public`** :material-lock-open-variant-outline: est visible et accessible par tous les utilisateurs. Un utilisateur non membre qui y accède pour la premiére fois en devient un membre `lecteur`, après avoir répondu oui à la pop-up d'accès.

Un espace **`modéré`** :material-lock-open-outline: est visible par tous les utilisateurs. Un utilisateur non membre qui essaie d'y accéder pour la premiére fois doit répondre oui à la pop-up de demande d'accès, le gestionnaire de l'espace sera alors notifié et choisira ou non d'accepter la demande à rejoindre. Rapprochez-vous du gestionnaire pour suivre votre demande.

Un espace **`privé`** :material-lock-outline: n'est visible que par les utilisateurs membres de l'espace.

!!! warning "Pristy Actes"
        Par défaut, tous les espaces Actes sont privés.

## Gérer les membres d'un espace

Consulter la page dédiée sur la [gestion des membres](../pristy/gestion-membres.md).

## Comprendre les rôles

Consulter la page dédiée sur les [rôles](../pristy/roles.md).

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Se repérer dans Pristy Espaces](se-reperer.md)
    - [Gestion des membres](../pristy/gestion-membres.md)
    - [Rôles](../pristy/roles.md)


!!! info inline end "Pour aller plus loin :"
    - [Gérer ses favoris](gerer-contenu/favoris.md)
    - [Corbeille](../portail/corbeille.md)

<!-- end Liens de bas de page -->
