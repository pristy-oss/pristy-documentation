---
title: Détails & métadonnées
description: Consulter les détails et métadonnées d'un élément.
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Détails et métadonnées

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [x] Pristy Espaces
      - [ ] Pristy Actes
      - [ ] Pristy Portail
      - [ ] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
      - [ ] Administrateur

---
!!!info "Pré-requis"
    - Aucun

<br> 
<br> 
<!-- end Prérequis -->


---

La zone d'information s'ouvre à droite de la page en appuyant sur le :material-information:{ .color-grey } (i) gris.

Dans l'onglet détail se trouve les informations sur l'emplacement dans lequel on se trouve. 

Les informations les plus courantes sont Nom, Titre, Description, Version, Auteur, Créé le, Modifié par, Modifié le, Taille. Les informations affichées peuvent être personnalisées par votre administrateur. 

Cet emplacement permet aussi d'afficher les métadonnées, types et les aspects spécifiques de l'élément, en fonction de la configuration décidé par l'administrateur de votre Pristy

<!-- Liens de bas de page -->

---

!!! summary inline "En lien avec cette page :"
        - [Se repérer dans Pristy Espaces](../se-reperer.md#zone-dinformations-metadonnees)


!!! info inline end "Pour aller plus loin :"
        - [Gestion de versions](../zone-information/gestion-versions.md)

<!-- end Liens de bas de page -->
