---
title: Gestion de versions d'un élément
description:
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->


<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [x] Pristy Espaces
      - [ ] Pristy Actes
      - [ ] Pristy Portail
      - [ ] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
      - [ ] Administrateur

---
!!!info "Pré-requis"
    - Aucun

<br> 
<br> 
<!-- end Prérequis -->

La zone d'information s'ouvre à droite de la page en appuyant sur le :material-information:{ .color-grey } (i) gris.

- En cliquant sur l'onglet version on peut afficher la liste des versions précédentes. 
- En cliquant sur le cercle bleu indiquant le numéro de version 
- À droite du cercle bleu se trouve la date et l'heure de la dernière version, pui sle nom de la personne qui a fait cette modification
- À droite de ces informations se trouve une icône "restaurer" :material-history: et l'icône "télécharger" :material-tray-arrow-down:


## Consulter les anciennes versions

Pour afficher les versions précédentes, il faut cliquer sur le cercle bleu indiquant le numéro de version. 

Un label vert avec le numéro de version s'affiche à côté du nom du document de la version consultée.

!!! tip "Partager une version"
    Le numéro de version est présent dans l'url : vous pouvez donc **partager aux membres de votre équipe un lien vers une version antérieure**. 

## Restaurer une ancienne version

Cliquer sur l'icône "restaurer" :material-history: en face du numéro de version souhaité. 

La version est restaurée dans une nouvelle version. 

## Télécharger une ancienne version


Cliquer sur l'icône "télécharger" :material-tray-arrow-down: en face du numéro de version souhaité.

Votre navigateur ouvre la fenêtre de téléchargement.

## Importer une nouvelle version

Dans le dossier où se situe le document :

- Cliquer sur le menu "Plus d'actions", représenté par trois points verticaux :material-dots-vertical:
- Sélectionner l'action ":material-tray-arrow-up: Importer une nouvelle version"
- La pop-up "Importer une nouvelle version" s'ouvre
- Sélectionner ou glisser/déposer la nouvelle version
- Ajouter un commentaire pour indiquer la modification apporté (facultatif)
- Indiquer s'il s'agit d'une version Majeure ou Mineure
  - Majeure changera le chiffre **avant** la virgule : 1.0 deviendra 2.0
  - Mineure changera le chiffre **après** la virgule : 1.0 deviendra 1.1
- Cliquez sur ":material-tray-arrow-up: Envoyer" dans la zone de fichier
- La nouvelle version est importée


!!! info "Documents éditables"
    Les documents éditables après avoir été modifié sont enregistrés en nouvelle version à chaque cloture du document ou enregistrement manuel dans Collabora.

!!! tip "OCR"
    Avec l'[option OCR](../../pristy/options/OCR.md), un document scanné, importé dans Pristy, sera transformé pour que vous puissiez *sélectionner* et *copier/coller* son contenu. Une nouvelle version sera alors créée : 1.1 il est alors plus facile de repérer qu'un document transformé. 
    Grâce à la gestion de version, vous avez toujours accès au document original.

!!! note "Copier un document avec des versions"
    Si vous copiez un document avec des versions antérieures, le document copié ne contiendra pas les précédentes versions. 

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Se repérer dans Pristy Espaces](../se-reperer.md#zone-dinformations-metadonnees)


!!! info inline end "Pour aller plus loin :"
    - [Zone d'information : détail](../zone-information/details.md)

<!-- end Liens de bas de page -->
