---
title: Se repérer dans un espace / dossier
description: Se repérer dans un dossier de Pristy Espaces
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

[//]: # (TODO 2023 - Créer visuel : avec encadré des sections et lien vers les pages liées.)

# Se repérer dans un espace / dossier

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [x] Pristy Espaces
      - [ ] Pristy Actes
      - [ ] Pristy Portail
      - [ ] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
      - [ ] Administrateur

---
!!!info "Pré-requis"
    - Aucun

<br> 
<br> 
<!-- end Prérequis -->

## Composition d'un dossier

Les espaces et les dossiers se présentent de la même façon, par simplicité de lecture nous ferons référence aussi aux deux cas en utilisant le mot "dossier".

### Zone d'actions & d'informations

En haut d'un dossier dans la zone grise, sous la barre d'en-tête, se trouver une zone qui donne des informations sur l'emplacement où l'on se trouver et permet différentes actions. 
Pour plus de simplicité nous l'appelons la **zone d'actions*.

#### Fil d'ariane (breadcrumb)

En haut de la zone d'action se trouve un fil d'ariane (aussi appelé breadcrumb, d'après le mot anglais), il vous indique ou vous vous trouvez. 

Tout à droite vous pouvez retournée à la page des espaces de travail en cliquant sur l'icône "maison". 

Entre l'icône "maison" et le dossier dans lequel vous vous trouvez se trouve tous les dossiers parents à votre emplacement. 

Vous pouvez cliquer dessus pour y accéder. 

#### Rôle & Accès

Sous le fil d'ariane, au gauche se trouve la mention du [rôle](../pristy/roles.md) que vous avez à cet emplacement.

À côte du rôle est indiqué le niveau d'accès possible pour l'emplacement (représenté par un cadenas plus ou moins ouvert).

#### Recherche

Au milieu de la zone d'action se trouve une barre de recherche, qui permet de chercher un contenu dans l'emplacement où l'on se trouve. 

Vous êtes alors renvoyé vers la page de recherche, avec l'emplacement filtré, par espace et par document. Vous pouvez retirer les filtre pour agrandir votre recherche. 

Pour rechercher directement dans tous Pristy Espaces, utiliser la recherche générale située dans la barre d'en-tête.

#### Ajouter du contenu

À droite du fil d'ariane, se trouve les [boutons créer et importer](ajouter-contenu/creer.md) qui permette d'ajouter du contenu. 

#### Sélectionner sa vue

À droite [boutons créer et importer](ajouter-contenu/creer.md), se trouve le sélecteur de vue. 

Voir la [section ci-dessous](#contenu) pour en savoir plus. 

#### Agir sur des documents sélectionner

En sélectionnant un ou plusieurs documents dans les listes vous pouvez actionner les actions suivantes : 

- [Obtenir un lien](../pristy/obtenir-lien.md) (fichiers uniquement)
- ~~Gestion de partage (désactivé)~~
- Télécharger (fichiers uniquemement)
- [Déplacer / Copier](gerer-contenu/copier-deplacer.md)
- Supprimer
- Plus d'action : 
  - [Ajouter/Retirer des Favoris](gerer-contenu/favoris.md)
  - Ouvrir (fichiers uniquement)

Se réferer aux pages liés pour en savoir plus

### Contenu

Sous la zone d'action se trouve un encadré blanc qui permet de voir, consulter et modifier son contenu. 

Sous toutes les vues se situe une zone pour sélectionner la page de consultation et le nombre de fichiers à afficher. 

#### Vue double

La vue double sépare les dossiers et les fichiers. 

Au-dessus, se situe la zone "Mes Dossiers" en vue block. 
Sur ces blocks se trouve une étoile :octicons-star-16: pour [Ajouter/Retirer un élément des Favoris](gerer-contenu/favoris.md) et un bouton plus d'action (trois points verticaux :material-dots-vertical:).

En-dessous, se situe la zone "Mes fichiers" en vue liste (se référer à la section [vue liste](#vue-liste)).

#### Vue liste

Une vue unique qui mélange dossier et fichiers. 
Par défaut, les dossiers s'affichent avant les fichiers, mais lors de tris, c'est tous les types se mélangent. 




#### Vue mosaïque

Cette fonctionnalité sera bientôt dans Pristy Espaces !


#### Dossier vide

Lorsqu'un dossier est vide, une vue avec un logo s'affiche et vous invite à déposer ou créer du contenu ? 

## Composition des pages de consultation

### Pdf

La page de consultation des fichiers aux formats pdf se compose d'un fil d'ariane (breadcrumb) puis de la zone pdf surplomber par une barre qui indique le nom de l'élément et qui reste constamment affiché pour gérer la taille du contenu, et ces actions : télécharger et imprimer, ouvrir le volet détail.

### Média

La page de consultation des fichiers aux formats pdf se compose d'un fil d'ariane (breadcrumb) puis de la zone pdf surplomber par une barre qui indique le nom de l'élément et qui reste constamment affiché pour gérer les actions : télécharger et ouvrir le volet détail.


### Fichier éditable (Collabora)

Les fichiers son éditable dans Collabora. 
L'interface de Collabora se compose
- en haut d'une barre d'actions
- au centre d'une zone pour ajouter / modifier du contenu
- en bas une barre qui inclut la recherche, la langue utilisé pour la rédaction du document et des informations sur le documents
- à droite s'ouvre le volet de style, il peut être fermé en cliquant sur le bouton violet du panneau latéral droit de collabora :material-dock-right:{.color-purple}

Consulter la page dédiée pour en savoir plus sur l'[édition en ligne](gerer-contenu/edition-en-ligne.md).

## Zone d'informations (métadonnées)

La zone d'information s'ouvre en appuyant sur le :material-information:{ .color-grey } (i) gris.

Elle s'ouvre à droite de la page.

Elle concerne l'élément que l'on consulte (pdf, media, document éditable, dossier)

Elle contient, aujourd'hui, deux onglets :

- [Détails](zone-information/details.md)
- [Versions](zone-information/gestion-versions.md)

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Bien débuter : Se repérer dans Pristy Espaces](bien-debuter.md#se-reperer-dans-pristy-espaces)
    - [Comprendre les espaces de travail](mes-espaces.md)

!!! info inline end "Pour aller plus loin :"
    - [Edition en ligne](gerer-contenu/edition-en-ligne.md)
    - [Gérer les favoris](gerer-contenu/favoris.md)
    - [Obtenir un lien](../pristy/obtenir-lien.md)
    - [Déplacer / Copier](gerer-contenu/copier-deplacer.md)
    - [Zone d'information : détails](zone-information/details.md)
    - [Gestion de versions](zone-information/gestion-versions.md)

<!-- end Liens de bas de page -->
