---
title: Supprimer un élément
description: Mettre un élément à la corbeille
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

[//]: # (TODO 2023 - Créer visuel : avec encadré des sections et lien vers les pages liées.)

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [x] Pristy Espaces
      - [ ] Pristy Actes
      - [ ] Pristy Portail
      - [ ] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
      - [ ] Administrateur

---
!!!info "Pré-requis"
    - Aucun

<br> 
<br> 
<!-- end Prérequis -->

## Supprimer un espace

Consulter la section dédiée sur la page des [espaces de travail](../mes-espaces.md#supprimer-un-espace-de-travail)

## Supprimer un élément

Pour supprimer un dossier, il faut en être créateur ou gestionnaire.
^^Attention^^, si vous supprimez un dossier, tout son contenu sera également supprimé, et ce, pour **tous les utilisateurs**.

- Cliquez sur les trois points verticaux :material-dots-vertical: au bout de la ligne de l'élément à supprimer
- Cliquez sur ":fontawesome-regular-trash-can: Supprimer"
- Confirmer la pop-up de validation

Le dossier est déplacé dans la [corbeille](../../portail/corbeille.md). 
Pour le supprimer définitivement, pensez à [vider votre corbeille](../../portail/corbeille.md#vider-la-corbeille). 

## Supprimer un document

Pour supprimer un document, il faut en être créateur ou gestionnaire.
^^Attention^^, si vous supprimez un document il sera supprimé pour **tous les utilisateurs**.

- Cliquez sur les trois points verticaux :material-dots-vertical: au bout de la ligne de l'élément à supprimer
- Cliquez sur ":fontawesome-regular-trash-can: Supprimer"
- Confirmer la pop-up de validation

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    * [Modifier](modifier.md)
    * [Copier/Déplacer](copier-deplacer.md)
    * [Favoris](favoris.md)
    * [Télécharger](telecharger.md)


!!! info inline end "Pour aller plus loin :"
    * [Zone d'information : version](../zone-information/gestion-versions.md)
    * [Comprendre les espaces de travail](../mes-espaces.md)
    * [Édition en ligne](edition-en-ligne.md)
    
<!-- end Liens de bas de page -->n/gestion-versions)
