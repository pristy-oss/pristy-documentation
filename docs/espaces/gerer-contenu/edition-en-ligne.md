---
title:  Édition en ligne
description:
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Édition en ligne

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [x] Pristy Espaces
    - [ ] Pristy Actes
    - [ ] Pristy Portail
    - [ ] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
    - [ ] Administrateur

---
!!!info "Pré-requis"
    - Aucun

<br> 
<br> 
<!-- end Prérequis -->


## Accéder à l'aide collabora online

Collabora Online est une suite bureautique intégrer à Pristy.
Elle a donc son aide dédiée.

Pour y accéder, il faut être dans un document modifiable dans Collabora puis cliquer sur le bandeau "Aide" puis sur le bandeau "Aide en ligne".

L'aide de Collabora s'affiche alors en pop-up.

!!! tip "Conseil"
      Si vous cherchez une fonctionnalité en particulier. Vous pouvez utiliser ctrl+f et rechercher un terme.

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
        - [Se repérer dans Collabora](../se-reperer.md#fichier-editable-collabora)
        - [Formats pris en charge](../formats-pris-en-charge.md)


!!! info inline end "Pour aller plus loin :"
        - [Gestion de versions](../zone-information/gestion-versions.md)

<!-- end Liens de bas de page -->
