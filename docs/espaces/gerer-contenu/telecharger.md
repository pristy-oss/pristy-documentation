---
title: Télécharger & imprimer un élément
description:
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

[//]: # (TODO 2023 - Créer visuel : avec encadré des sections et lien vers les pages liées.)

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [x] Pristy Espaces
      - [ ] Pristy Actes
      - [ ] Pristy Portail
      - [ ] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
      - [ ] Administrateur

---
!!!info "Pré-requis"
    - Aucun

<br> 
<br> 
<!-- end Prérequis -->

## Télécharger un dossier

Ce n'est pas aujourd'hui possible

## Télécharger un document

### Dans un dossier
Tous les rôles peuvent télécharger un document

- Cliquez sur les trois points verticaux :material-dots-vertical: au bout de la ligne de l'élément à télécharger
- Cliquez sur ":material-tray-arrow-down: Télécharger"
- Suivre les indications de votre navigateur pour enregistrer le pdf

### Sur la page de consultation des documents non modifiable

- Sur la barre de gestion de la vue
- Cliquer sur le bouton jaune :material-tray-arrow-down:{ .color-bg-yellow .button} "Télécharger"
- Suivre les indications de votre navigateur pour enregistrer le pdf

### Sur la page de consultation des documents modifiable (dans Collabora)

- Dans la vue Collabora
- Choisir l'onglet "Fichier"
- Cliquer sur "Imprimer"
- Suivre les indications de votre navigateur pour enregistrer le pdf

## Imprimer un document

### Sur la page de consultation des documents non modifiable

- Sur la barre de gestion de la vue
- Cliquer sur le bouton jaune :material-printer-outline:{ .color-bg-yellow .button} "Imprimer"
- Suivre les indications de votre navigateur pour imprimer le document

### Sur la page de consultation des documents modifiable (dans Collabora)

- Dans la vue Collabora
- Choisir l'onglet "Fichier"
- Cliquer sur "Imprimer"
- Suivre les indications de votre navigateur pour enregistrer le pdf
- Ouvrir le pdf sur votre pc et l'imprimer comme n'importe quelle autre document

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Modifier](modifier.md)
    - [Copier/Déplacer](copier-deplacer.md)
    - [Supprimer](supprimer.md)
    - [Favoris](favoris.md)


!!! info inline end "Pour aller plus loin :"
    - [Zone d'information : version](../zone-information/gestion-versions.md)
    - [Comprendre les espaces de travail](../mes-espaces.md)
    - [Édition en ligne](edition-en-ligne.md)
    
<!-- end Liens de bas de page -->
