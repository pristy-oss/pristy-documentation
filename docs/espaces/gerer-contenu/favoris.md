---
title: Ajouter / Supprimer des favoris
description: Ajouter / supprimer des éléments des favoris
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Ajouter / Supprimer des favoris

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [x] Pristy Espaces
      - [ ] Pristy Actes
      - [ ] Pristy Portail
      - [ ] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
      - [ ] Administrateur

---
!!!info "Pré-requis"
    - Cette étape suppose que vous vous êtes connecté à Pristy et avez accès à au moins un espace de travail contenant des dossiers et/ou fichiers.
    Si ce n'est pas le cas vous [créer un espace de travail]() et [ajouter des dossiers]() ainsi qu'[importer vos fichiers]().

<br> 
<!-- end Prérequis -->

  

## Espaces favoris

Vous pouvez suivre les instructions suivantes pour ajouter/supprimer un espace des favoris :

* Accéder à `votre-nom-de-domaine.com/espaces/mes-espaces`
* Cliquer sur l'étoile verte :octicons-star-16:{ .color-green } en haut à droite de l'espace souhaité

!!! info "Gérer ses espaces favoris"
    Si l'étoile :octicons-star-fill-16:{ .color-green } est pleine, vous venez d'**ajouter** votre espace **aux favoris** et il apparaîtra dans le menu à gauche sous l'élément "Espaces de travail".

    Si l'étoile :octicons-star-16:{ .color-green } est vide, vous venez de **supprimer** votre espace **des favoris** et il disparaîtra du menu à gauche. Cependant il sera toujours visible sur la page des Espaces.


## Fichiers / Dossiers favoris

Vous pouvez suivre les instructions suivantes pour ajouter/supprimer un fichier ou dossier des favoris :

* Accéder à `votre-nom-de-domaine.com/espaces/mes-espaces`
* Choisir un espace
* Cliquer sur l'étoile :octicons-star-16: à droite sur la ligne du fichier ou dossier souhaité

!!! info "Gérer ses fichiers / dossiers favoris"
    Si l'étoile :octicons-star-fill-16: est pleine, vous venez d'**ajouter** votre fichier/dossier **aux favoris** et il apparaîtra dans le menu à gauche sous l'élément "Favoris".
    
    Si l'étoile :octicons-star-16: est vide, vous venez de **supprimer** votre fichier/dossier **des favoris** et il disparaîtra du menu à gauche. Cependant vous y aurez toujours accès en passant par la navigation dans vos espaces/dossiers.

## Menu de favoris

Vous retrouvez vos éléments favoris dans votre menu (:material-menu:) à gauche.

Vous pouvez ainsi les retrouver facilement depuis n'importe où dans Pristy !

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Modifier](modifier.md)
    - [Copier/Déplacer](copier-deplacer.md)
    - [Supprimer](supprimer.md)
    - [Télécharger](telecharger.md)


!!! info inline end "Pour aller plus loin :"
    - [Zone d'information : version](../zone-information/gestion-versions.md)
    - [Comprendre les espaces de travail](../mes-espaces.md)
    - [Édition en ligne](edition-en-ligne.md)
    
<!-- end Liens de bas de page -->
