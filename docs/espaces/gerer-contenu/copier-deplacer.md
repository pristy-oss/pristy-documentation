---
title: Copier / Déplacer
description: Copier et déplacer des éléments
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Copier / Déplacer

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [X] Pristy Espaces
    - [ ] Pristy Actes
    - [ ] Pristy Portail
    - [ ] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
    - [ ] Administrateur

---
!!!info "Pré-requis"
        Cette étape suppose que vous vous êtes connecté à Pristy et avez accès à au moins un espace de travail contenant des dossiers et/ou fichiers.
    Si ce n'est pas le cas vous [créer un espace de travail]() et [ajouter des dossiers]() ainsi qu'[importer vos fichiers]().
     
<br> 
<!-- end Prérequis -->

## Copier

Vous pouvez suivre les instructions suivantes pour copier un fichier ou un dossier :

* Accéder à l'espace de travail de votre choix
* Sur le dossier/fichier de votre choix, cliquer sur le menu "Plus d'actions", représenté par trois points verticaux :material-dots-vertical:
* Sélectionner l'action ":material-file-multiple-outline: Copier"
* Sélectionner la destination souhaitée et **valider**

!!! warning "Droit d'écriture requis"
      Attention, si vous n'avez pas les droits en écriture **à la destination**, vous ne pourrez pas "Copier" l'élément.



## Déplacer

Vous pouvez suivre les instructions suivantes pour déplacer un fichier ou un dossier :

* Accéder à l'espace de travail de votre choix
* Sur le dossier/fichier de votre choix, cliquer sur le menu "Plus d'actions", représenté par trois points verticaux :material-dots-vertical:
* Sélectionner l'action ":material-arrow-right-top: Déplacer"
* Sélectionner la destination souhaitée et **valider**

!!! warning "Droit d'écriture requis"
    Attention, l'action "Déplacer" ne sera pas visible si vous n'avez pas les droits en écriture **dans l'espace où les éléments se trouvent**. Si vous n'avez pas les droits en écriture **à la destination**, vous ne pourrez pas "Déplacer" l'élément.


<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Modifier](modifier.md)
    - [Favoris](favoris.md)
    - [Supprimer](supprimer.md)
    - [Télécharger](telecharger.md)


!!! info inline end "Pour aller plus loin :"
    - [Zone d'information : version](../zone-information/gestion-versions.md)
    - [Comprendre les espaces de travail](../mes-espaces.md)
    - [Édition en ligne](edition-en-ligne.md)
    
<!-- end Liens de bas de page -->
