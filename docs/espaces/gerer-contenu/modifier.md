---
title: Modifier un élément : renommer, description
description: Modifier un élément : le renommer et gérer sa description & autres métadonnées
---

<!--
  Copyright 2025 - Jeci SARL - https://jeci.fr

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.3
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
  A copy of the license is included in the section entitled "GNU
  Free Documentation License".

  You should have received a copy of the GNU Free Documentation License
  along with this program.  If not, see http://www.gnu.org/licenses/.
-->

# Utiliser l'action modifier 

<!-- Prérequis -->

!!! check inline "Présent dans l'application"
    - [x] Pristy Espaces
      - [ ] Pristy Actes
      - [ ] Pristy Portail
      - [ ] Pristy app content


!!! check inline end "Droit nécessaire"
    - [x] Utilisateur (lecteur / éditeur / gestionnaire)
      - [ ] Administrateur

---
!!!info "Pré-requis"
    - Aucun

<br> 
<br> 
<!-- end Prérequis -->

L'action modifier permet d'abord de renommer un élément et de lui écrire une description.

## Modifier un espace

Consulter la section dédiée sur la page des [espaces de travail](../mes-espaces.md#modifier-un-espace)

## Modifier un dossier

Pour modifier un dossier, il faut en être créateur, éditeur ou gestionnaire.

- Cliquez sur les trois points verticaux :material-dots-vertical: au bout de la ligne de l'élément à modifier
- Cliquez sur ":octicons-pencil-24: Modifier"
- Vous pouvez modifier le nom et la description du dossier.

## Modifier un document

Pour modifier un document, il faut en être créateur, éditeur ou gestionnaire.

- Cliquez sur les trois points verticaux :material-dots-vertical: au bout de la ligne de l'élément à modifier
- Cliquez sur ":octicons-pencil-24: Modifier"
- Vous pouvez modifier le nom, la description du dossier, son titre et son auteur

<!-- Liens de bas de page -->

---
!!! summary inline "En lien avec cette page :"
    - [Favoris](favoris.md)
    - [Copier/Déplacer](copier-deplacer.md)
    - [Supprimer](supprimer.md)
    - [Télécharger](telecharger.md)


!!! info inline end "Pour aller plus loin :"
    - [Zone d'information : version](../zone-information/gestion-versions.md)
    - [Comprendre les espaces de travail](../mes-espaces.md)
    - [Édition en ligne](edition-en-ligne.md)
    
<!-- end Liens de bas de page -->
