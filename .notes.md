# Note pour les mises à jours de la documentation Pristy 

Last update : 2024-01-31

- Todos ajoutés au dossier pristy
- Todo à ajouter : 
  - Dossier Espaces
  - Dossier Portail
  - Dossier Actes 
    - \+ rédaction de la doc de consultation ! 
  - Dossier Installation 
- Todo : mettre à jour les visuels
- Todo : créer les visuels d'emplacement

Ordonner les priorités !